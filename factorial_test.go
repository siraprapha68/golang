package main

import "testing"

func BenchmarkLoop(b *testing.B) {
	for index := 0; index < b.N; index++ {
		facLoop(0)
		facLoop(5)
		facLoop(20)
		facLoop(50)
	}
}

func BenchmarkRecurs(b *testing.B) {
	for index := 0; index < b.N; index++ {
		facRecursion(0)
		facRecursion(5)
		facRecursion(20)
		facRecursion(50)
	}
}

func BenchmarkRecursTail(b *testing.B) {
	for index := 0; index < b.N; index++ {
		facRecursionTail(0)
		facRecursionTail(5)
		facRecursionTail(20)
		facRecursionTail(50)
	}
}