package main

func facLoop(num int) int {
	result := 1
	for ; num > 1; num-- {
		result *= num
	}
	return result
}

func facRecursion(num int) int {
	if num < 2 {
		return 1;
	}
	return num*facRecursion(num-1)
}

func facRecursionTail(num int) int {
	return facRecursionTail2(num, 1)
}
func facRecursionTail2(num int, result int) int {
	if num < 2 {
		return result
	} else {
		return facRecursionTail2(num-1, result*num)
	}
}