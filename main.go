package main

import (
	"fmt"
	"github.com/logrusorgru/aurora"
)

func main() {
	greetingMsg := greeting("Inky")
	fmt.Println(aurora.Blue(greetingMsg))

	q := 5
	fmt.Println(aurora.Green(fmt.Sprintf("factorial loop of %v is %v",q,facLoop(q))))
	fmt.Println(aurora.Magenta(fmt.Sprintf("factorial recursion of %v is %v",q,facRecursion(q))))
	fmt.Println(aurora.Cyan(fmt.Sprintf("factorial recursion tail of %v is %v",q,facRecursionTail(q))))
}