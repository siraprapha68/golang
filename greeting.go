package main

import "fmt"

func greeting(name string) string  {
	if len(name) == 0 {
		return "Who?"
	} else {
		return fmt.Sprintf("Hello %v!", name);
	}
}