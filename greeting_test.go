package main

import "testing"

func TestGreeting( t *testing.T)  {
	emptyName := greeting("")
	if emptyName != "Who?" {
		t.Errorf("greeting(\"\") failed, expected %v, got %v", "Who?", emptyName)
	} else {
		t.Logf("greeting(\"\") success, expected %v, got %v", "Who?", emptyName)
	}

	name := greeting("Inkieeëę")
	if name != "Hello Inkieeëę!" {
		t.Errorf("greeting(\"\") failed, expected %v, got %v", "Hello Inkieeëę!", name)
	} else {
		t.Logf("greeting(\"\") success, expected %v, got %v", "Hello Inkieeëę!", name)
	}
}

func TestGreetingValidName(t *testing.T) {
	name := greeting("Inkieeëę")
	if name != "Hello Inkieeëę!" {
		t.Errorf("greeting(\"\") failed, expected %v, got %v", "Hello Inkieeëę!", name)
	} else {
		t.Logf("greeting(\"\") success, expected %v, got %v", "Hello Inkieeëę!", name)
	}
}